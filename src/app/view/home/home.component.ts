import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  title: string;

  setTitle() {
    const date = new Date();
    this.title = `Calculadora de Salário Líquido ${date.getFullYear().toString()}`;
  }

  addDiscount() {
    const descriptionDiscountElement =   document.getElementById('description-discount') as HTMLInputElement;
    const descriptionDiscount =  descriptionDiscountElement.value;

    const valueDiscountElement =  document.getElementById('value-discount') as HTMLInputElement;
    const valueDiscount = parseFloat(valueDiscountElement.value.replace(',', '.'));

    if (!valueDiscount || valueDiscount == null || valueDiscount <= 0 || isNaN(valueDiscount)) {
        return;
    }

    const elementDiscounts = document.getElementById('insert-info-discounts')  as HTMLInputElement;
    const countDiscountsItems = document.querySelectorAll('.discount-item').length;

    const divElementDiscount = document.createElement('div');
    divElementDiscount.id = `element-discount-${countDiscountsItems}`;
    divElementDiscount.classList.add('col-10', 'dflex-row', 'card-discount-item');

    const spanDiscount = document.createElement('span');
    spanDiscount.classList.add('col-6');
    spanDiscount.innerHTML = `${descriptionDiscount}`;
    divElementDiscount.appendChild(spanDiscount);

    const inputDiscount = document.createElement('input');
    inputDiscount.type = 'text';
    inputDiscount.id = `discount-${countDiscountsItems}`;
    inputDiscount.classList.add('col-2', 'discount-item');
    inputDiscount.value = `${valueDiscount}`;
    divElementDiscount.appendChild(inputDiscount);

    const divRemove = document.createElement('div');
    divRemove.classList.add('remove-item');
    divRemove.id = `element-discount-${countDiscountsItems}`;
    divRemove.innerHTML = 'X';
    divRemove.addEventListener('click', () => {
      this.removeDiscount(divRemove.id);
    });
    divElementDiscount.appendChild(divRemove);

    elementDiscounts.appendChild(divElementDiscount);
  }

  removeDiscount(idElementDiscount: string) {
    document.getElementById(idElementDiscount).remove();
  }

  validateMoney(e) {
    if (!(e.keyCode >= 48 && e.keyCode <= 57) && e.keyCode !== 8 && e.keyCode !== 190 && e.keyCode !== 188) {
      const removeValue = e.originalTarget.value.slice(0, -1);
      e.originalTarget.value = removeValue;
    }
  }

  private calcDiscountINSS(salary) {
    const resultINSS = {
      discount: 0,
      ref: '0'
  };

    if (salary <= 1659.38) {
      resultINSS.discount = salary * 8 / 100;
      resultINSS.ref = '8';
  } else if (salary <= 2765.66) {
      resultINSS.discount = salary * 9 / 100;
      resultINSS.ref = '9';
  } else if (salary <= 5531.31) {
      resultINSS.discount = salary * 11 / 100;
      resultINSS.ref = '11';
  } else {
      resultINSS.discount = 604.44;
      resultINSS.ref = '- ';
  }

    return resultINSS;
  }

  private calcDiscountIRRF(salaryBase, numDependents) {
    const salaryBaseIRRF = salaryBase - numDependents * 189.59;

    const resultIRRF = {
        discount: 0,
        ref: 0
    };

    if (salaryBaseIRRF < 1903.99 ) {
        return resultIRRF;
    }

    if (salaryBaseIRRF <= 2826.65) {
        resultIRRF.discount = salaryBaseIRRF * 7.5 / 100 - 142.80;
        resultIRRF.ref = 7.5;
    } else if (salaryBaseIRRF <= 3751.05) {
        resultIRRF.discount = salaryBaseIRRF * 15 / 100 - 354.80;
        resultIRRF.ref = 15;
    } else if (salaryBaseIRRF <= 4664.68) {
        resultIRRF.discount = salaryBaseIRRF * 22.5 / 100 - 636.13;
        resultIRRF.ref = 22.5;
    } else if (salaryBaseIRRF >= 4664.69) {
        resultIRRF.discount = salaryBaseIRRF * 27.5 / 100 - 869.36;
        resultIRRF.ref = 27.5;
    }

    return resultIRRF;
  }

  calcLiquidSalary() {
    const salaryInputElement = document.getElementById('salary') as HTMLInputElement;
    const salary = parseFloat(salaryInputElement.value.replace(',', '.'));

    if (isNaN(salary)) {
      salaryInputElement.value = '';
      return;
    }

    if (!salary || salary == null || salary <= 0) {
        return;
    }

    const nDependentsElement = document.getElementById('dependents') as HTMLInputElement;
    const nDependents = nDependentsElement.value;

    const resultINSS = this.calcDiscountINSS(salary);
    const salaryBase = salary - resultINSS.discount;
    const resultIRRF = this.calcDiscountIRRF(salaryBase, nDependents);

    const cardDiscountItems = document.querySelectorAll('.card-discount-item');


    let liquidSalary = salary - resultINSS.discount - resultIRRF.discount;

    cardDiscountItems.forEach(cardDiscountItem => {
        const elementInputDiscount: any = cardDiscountItem.children[1] as HTMLInputElement;
        liquidSalary -= elementInputDiscount.value;
    });

    let stringResult = `<h1 class="col-10">Resultado</h1>
                        <table class="col-9 table">

                            <tr>
                                <th>
                                    Descrição
                                </th>
                                <th>
                                    Ref.
                                </th>
                                <th>
                                    Valor
                                </th>
                            </tr>`;
    const date = new Date();
    stringResult += this.createTrResponse('Salário Bruto', '-', salary, 'add-value-table');
    stringResult += this.createTrResponse('INSS', `${resultINSS.ref}%`, resultINSS.discount, 'discount-table');
    stringResult += this.createTrResponse(`IRRF ${date.getFullYear()}`, `${resultIRRF.ref}%`, resultIRRF.discount, 'discount-table');
    cardDiscountItems.forEach(cardDiscountItem => {
        const spanElementDiscount = cardDiscountItem.children[0];
        const divElementDiscount = cardDiscountItem.children[1] as HTMLInputElement;
        stringResult += this.createTrResponse(spanElementDiscount.innerHTML, '-', divElementDiscount.value, 'discount-table');
    });

    stringResult += this.createTrResponse('Salário Líquido', '-', liquidSalary, 'add-value-table');
    stringResult += '</table>';

    document.getElementById('result').innerHTML = stringResult;
  }

  private createTrResponse(description, ref, value, classTdValue) {
    const trString = `<tr>
                        <td>
                            ${description}
                        </td>
                        <td>
                            ${ref}
                        </td>
                        <td class="${classTdValue}">
                            ${classTdValue == 'discount-table' ? '-' : ''}${parseFloat(value).toFixed(2)}
                        </td>
                    </tr>`;
    return trString;
  }

  resetPage() {
    window.location.reload();
  }
  constructor() { }

  ngOnInit() {
    this.setTitle();
  }
}
